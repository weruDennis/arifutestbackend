<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Storage;
use App\Student;
use App\Course;

class StudentController extends Controller
{   
    function post_details(Request $request) {
        if ($request) {
            $student = Student::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'city' => $request->city,
                'country' => $request->country,
                'course' => $request->course,
                'postal_address' => $request->postal_address,
                'email' => $request->email
            ]);

            if ($student) {
                return response()->json(1); //success
            } else {
                return response()->json(504); //error
            }
            // 
        }
    }

    public function get_details(Request $request) {
        if ($request) {
            $students = \DB::table('students')->orderby('created_at','desc')->get()->toArray();
            return response()->json($students); //success
        } else {
            return response()->json(504); //error
        }
    }

    function post_course(Request $request) {
        if ($request) {
            $course = Course::create([
                'course_name' => $request->course_name,
            ]);

            if ($course) {
                return response()->json(1); //success
            } else {
                return response()->json(504); //error
            }
            // 
        }
    }

    public function get_courses(Request $request) {
        if ($request) {
            $courses = \DB::table('courses')->orderby('created_at','desc')->get()->toArray();
            return response()->json($courses); //success
        } else {
            return response()->json(504); //error
        }
    }

    public function get_blogs(Request $request, $criteria,$total,$id) {
        if ($criteria && $total) {
            if ($criteria == "time") {
                if($total == 'infinite') { //get all blogs
                    $blogs = \DB::table('blogs')->orderby('created_at','desc')->get()->toArray();
                } else { //get 'total' number of blogs
                    if ($id == "none") { //select from top of table
                        $blogs = \DB::table('blogs')->orderby('created_at','desc')->limit($total)->get()->toArray();
                    }
                    else if ($id == "inverted") { //select from bottom of the table
                        $blogs = \DB::table('blogs')->orderby('created_at','asc')->limit($total)->get()->toArray();
                    }
                    else if ($id ==  "page") { //If this is true then the $total parameter becomes the page number
                        $items = ($total-1) * 9;
                        $topblogs = \DB::table('blogs')->orderby('created_at','desc')->limit($items)->get()->toArray(); //select from top and get the items in pages below current page no in '$id'
                        $last_id = count($topblogs) - 1;
                        $blog_last = $topblogs[$last_id];
                        $blog_last_arr = (array) $blog_last;
                        $blog_last_id =  $blog_last_arr['id']; //get last id in topblogs
                        $blogs = \DB::table('blogs')->where('id','<',$blog_last_id)->orderby('created_at','desc')->limit(9)->get()->toArray(); //select from the last id downwards
                    }
                    else { //select from 'id' id downwards
                        $blogs = \DB::table('blogs')->where('id','<',$id)->orderby('created_at','desc')->limit($total)->get()->toArray();
                    }
                    // $blogs = \DB::table('blogs')->orderby('created_at','desc')->limit($total)->get()->toArray();
                }
                $theblogs = [];
                for ($i = 0; $i < count($blogs); $i++) { //fix image links
                    $blog = (array) $blogs[$i];                    
                    if ($image = $blog['image']) {
                        $fullimage = 'file_uploads'."'".'images'."'".$image;                       
                        $fullimage =  addslashes($fullimage);
                        $fullimage = str_replace("'" , "",$fullimage);
                        $blog['image'] = str_replace("'" , "",$fullimage);
                    }
                    array_push($theblogs, $blog);
                }
                return response()->json($theblogs);
            }
        } else {
            return response()->json('Incomplete request');
        }
    }
    
    public function get_featured_blogs () {
        $blogs = \DB::table('blogs')->where('featured','=','featured')->get()->toArray();
        $theblogs = [];
            for ($i = 0; $i < count($blogs); $i++) { //fix image links
                $blog = (array) $blogs[$i];                    
                if ($image = $blog['image']) {
                    $fullimage = 'file_uploads'."'".'images'."'".$image;                       
                    $fullimage =  addslashes($fullimage);
                    $fullimage = str_replace("'" , "",$fullimage);
                    $blog['image'] = str_replace("'" , "",$fullimage);
                }
                array_push($theblogs, $blog);
            }
        return response()->json($theblogs);
    }
    public function get_total_blogs () {
        $blogs_count = \DB::table('blogs')->count();
        return response()->json($blogs_count);
    }
    public function get_blogs_by_author(Request $request, $author) {
        if ($author) {
            $blogs = \DB::table('blogs')->where('author','=',$author)->get()->toArray();
            $theblogs = [];
                for ($i = 0; $i < count($blogs); $i++) { //fix image links
                    $blog = (array) $blogs[$i];                    
                    if ($image = $blog['image']) {
                        $fullimage = 'file_uploads'."'".'images'."'".$image;                       
                        $fullimage =  addslashes($fullimage);
                        $fullimage = str_replace("'" , "",$fullimage);
                        $blog['image'] = str_replace("'" , "",$fullimage);
                    }
                    array_push($theblogs, $blog);
                }
            return response()->json($theblogs);
            } else {
                return response()->json('Incomplete request parameters');
        }
    }
    public function edit_blog(Request $request, $id)
    {
        if ($id) {
            $single_blog = Blog::find($id);
            $newbody = $request->body;
            $newtitle = $request->title;
            $newauthor = $request->author;
            $newexcerpt = $request->excerpt;
            $newimage = "";
            if ($file = $request->file('image')) {
                $str = rand(); 
                $random = hash("sha256", $str); 
                $upload = $request->file('image')->move('file_uploads/images', $newimage = $random.'_'.str_replace(" " , "_", $file->getClientOriginalName()));
                // $poster_request = $request->file('image')->store(base_path().'/public/file_uploads/images', $file_name = '/file_uploads/images/this.jpg'); 
                //return response()->json(1); 
            }

            //remove the oldest featured blog
            if ($request->featured == "featured") {
                $featured_blogs = \DB::table('blogs')->where('featured','=', 'featured')->orderby('updated_at','desc')->get()->toArray();
                $total = (array) $featured_blogs;
                $i = count($total);           
                if ($i > 2) {
                    if ($last_blog = (array) $featured_blogs[2]){
                        $id = $last_blog['id'];
                        \DB::table('blogs')->where('id', $id)->update(['featured' => '']);
                    }
                }
            }
            // $blog_post = \DB::table('blogs')->where('id','=',$id)->get()->toArray();
            if ($newimage) {
                \DB::table('blogs')
                ->where('id', $id)
                ->update(['image' => $newimage]);
            }
            \DB::table('blogs')
            ->where('id', $id)
            ->update(['body' => $newbody, 'title' => $newtitle, 'excerpt' => $newexcerpt, 'author' => $newauthor]);
            return response()->json(1);
        } else {
            return response()->json("ID not defined");
        }
    }
    
}