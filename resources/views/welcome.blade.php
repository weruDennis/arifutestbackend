<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Arifu</title>

    </head>
    <body class="antialiased" style="display:flex; justify-content:center">
        <img src="{{ asset('images/ArifuLogo.png') }}" alt="">
    </body>
</html>
